import java.util.Scanner;

class Program {
	public static double f(double x) {
		return x * x;
	} 

	public static double calcIntegral(double a, double b, int n) {
		double h = (b - a) / n;
		double result = 0;
		for (double x = a; x <= b; x += h) {
			double reclangleArea = f(x) * h;
			result += reclangleArea;
		}

		return result;
	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		double a = scanner.nextDouble();
		double b = scanner.nextDouble();
		int n = scanner.nextInt();

		double integral = calcIntegral(a, b, n);

		System.out.println(integral);
	}
}