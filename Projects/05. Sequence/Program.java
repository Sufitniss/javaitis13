import java.util.Scanner;

class Program {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int currentNumber = scanner.nextInt();

		int evenNumbersCount = 0;
		int oddNumbersCount = 0;

		while(currentNumber != -1) {
			if (currentNumber % 2 == 0) {
				evenNumbersCount++;
			} else {
				oddNumbersCount++;
			}
			currentNumber = scanner.nextInt();
		}	
		System.out.println(evenNumbersCount);
		System.out.println(oddNumbersCount);	
	}
}