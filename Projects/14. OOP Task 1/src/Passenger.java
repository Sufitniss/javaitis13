/**
 * 20.02.2021
 * 14. OOP Task 1
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Passenger {
    private String firstName;
    private String lastName;

    public Passenger(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public void goToBus(Bus bus) {
        bus.addPassenger(this);
    }

    public String getFirstName() {
        return firstName;
    }
}
