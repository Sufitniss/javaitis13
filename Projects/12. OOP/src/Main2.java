/**
 * 10.02.2021
 * 12. OOP
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main2 {
    public static void main(String[] args) {
        User marsel = new User();

        User victor = new User("Виктор");
        victor.password = "qwerty007";
        victor.height = 1.76;
        victor.age = 24;

        User anotherVictor = new User(victor);
    }
}
