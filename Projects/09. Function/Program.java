import java.util.Arrays;
class Program {

	public static boolean isPrime(int number) {
		if (number == 2 || number == 3) {
			return true;
		}

		for (int i = 2; i * i <= number; i++) {
			if (number % i == 0) {
				return false;
			}
		}

		return true;
	}

	public static int getSumInRange(int from, int to) {
		int result = 0;

		for (int i = from; i <= to; i++) {
			result += i;
		}

		return result;
	}

	public static void fillZero(int array[]) {
		for (int i = 0; i < array.length; i++) {
			array[i] = 0;
		}
	}

	public static void printInRange(int from, int to) {
		for (int i = from; i <= to; i++) {
			System.out.print(i + " ");
		}
		System.out.println();
	} 

	public static void main(String[] args) {
		printInRange(1, 10);
		printInRange(5, 15);

		int sum = getSumInRange(5, 10);
		int sum2 = getSumInRange(1, 100);

		System.out.println(sum);

		System.out.println(isPrime(7));
		System.out.println(isPrime(121));
		System.out.println(isPrime(167));

		int numbers[] = {1, 2, 3, 4, 5};
		fillZero(numbers);
		
		System.out.println(Arrays.toString(numbers));	
	}
}