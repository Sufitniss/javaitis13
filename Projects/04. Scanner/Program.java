import java.util.Scanner;

class Program {
	public static void main(String[] args) {
		// myScanner -> variable
		// Scanner -> type
		Scanner myScanner = new Scanner(System.in);
		int firstNumber = myScanner.nextInt();
		int secondNumber = myScanner.nextInt();
		System.out.println(firstNumber + secondNumber);
	}
}