class Program {
	public static void main(String[] args) {
		Human marsel = new Human();
		Human victor = new Human();

		marsel.age = 27;
		victor.age = 24;

		marsel.height = 1.85;
		victor.height = 1.76;

		marsel.grow(0.03);

		System.out.println(marsel.height);
	}
}