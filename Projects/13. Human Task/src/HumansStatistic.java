/**
 * 10.02.2021
 * 13. Human Task
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// класс, отвечающий за формирование статистики
public class HumansStatistic {
    int[] getAgesStatistic(Human humans[]) {
        int ages[] = new int[100];

        for (int i = 0; i < humans.length; i++) {
            Human currentHuman = humans[i];
            // currentHumanAge = 81
            int currentHumanAge = humans[i].getAge();
            // ages[81]++
            ages[currentHumanAge]++;
        }

        return ages;
    }
}
